import React, { Component } from 'react'
import axios from 'axios'

class App extends Component{
    constructor() {
        super()
        this.state = {user:[]}
    }

    getUser =  async () =>{
        let response = await axios.get('https://jsonplaceholder.typicode.com/users')
        this.setState({
            user: response.data
        })
    }

    componentDidMount(){
        this.getUser()
    }

    render(){
        const {user} = this.state
        return(
            <div className="py-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Website</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        {
                                            user.map((userx, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>{userx.name}</td>
                                                        <td>{userx.email}</td>
                                                        <td>{userx.website}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default App;