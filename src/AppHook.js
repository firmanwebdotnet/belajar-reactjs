import React, {useState, useEffect} from 'react'
import axios from 'axios'

function AppHook(){
    const [user,setUser] = useState([])
    const getUser = async () => {
        try {
            let response = await axios.get('https://jsonplaceholder.typicode.com/users')
            setUser(response.data)
        } catch(e){
            console.log(e.message);
        }
    }

    useEffect(() => {
        getUser();
    }, [])

            return(
            <div className="py-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Website</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        {
                                            user.map((userx, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>{userx.name}</td>
                                                        <td>{userx.email}</td>
                                                        <td>{userx.website}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            )
        }
export default AppHook;